# AutoResize
Автоматическое увеличение textarea


## Installation
```js
jQuery(document).ready(function($) {
    $('textarea').autoResize();
});
```

<br><hr>

<div align="center">Разработано для <a href="https://iPlayCraft.ru/">iPlayCraft.ru</a><br>2018 год</div>
