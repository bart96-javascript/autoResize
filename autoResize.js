/* Автор - Пряхин Игорь  ::  BART96  ::  Author - Prjakhin Igor */
/* Уважайте чужой труд.  ::  Respect other peoples work. */

/**
 *  @version 1.0.0 (2018.04.27)
 *  @author BART96
 *  @name AutoResize
**/



$('head').eq(0).append(`
	<style>
		.charLength {float:right; position:relative; opacity:.1; transition:.5s;}
		.charLength.scrollWidth {right:23px;}
		.charLength:hover,
		textarea:hover ~ .charLength {opacity:1;}
		textarea:focus ~ .charLength {color:#4d90fe;}
		textarea.overflowY {overflow-y:auto !important;}
	</style>
`);



(function($) {
	$.fn.autoResize = function(options) {
		let settings = $.extend({
			onResize: function() {},
			limit: 1000,
			extraSpace: 40,
			animate: {
				enable: true,
				duration: 150,
				callback: function() {},
			},
		}, options);

		this.filter('textarea').each(function() {
			let textarea = $(this).css({resize:'none', overflowY:'hidden', display:'block', transition:'none'});
			let lastScrollTop = null;

			let clone = (() => {
				let addStyle = {top:0, left:-9999, position:'absolute'};
				let cloneStyle = ['height', 'width', 'lineHeight', 'textDecoration', 'letterSpacing'].reduce((json, prop) => {
					return json[prop] = textarea.css(prop), json;
				}, Object.create(null));

				return textarea.clone().removeAttr('id name placeholder').attr('tabIndex', '-1').css(addStyle).css(cloneStyle).insertAfter(textarea);
			})();

				let charLength = (() => {
				return $('<div>', {
					class: 'charLength',
					html: `Символов: <b>${textarea.val().length}</b>`,
					css: {
						right: '7px',
						top: - textarea.css('marginBottom').slice(0, -2) - textarea.css('borderBottomWidth').slice(0, -2) - 20 +'px',
					}
				}).insertAfter(textarea);
			})();

			let updateSize = function() {
				let thisArea = $(this);
				let scrollW = clone.height(0).val(thisArea.val()).scrollTop(1e4).scrollTop() + settings.extraSpace;

				charLength.children('b').text(thisArea.val().length);

				if (lastScrollTop == scrollW) return;
				else lastScrollTop = scrollW;

				if (thisArea.outerWidth() - parseInt(thisArea.css('borderWidth')) * 2 == thisArea.prop("scrollWidth")) charLength.css({right:'7px'});
				else charLength.css('right', thisArea.outerWidth() - parseInt(thisArea.css('borderWidth')) * 2 - thisArea.prop("scrollWidth") + 7 +'px');

				if (clone.scrollTop() + settings.extraSpace >= settings.limit) {
					if (thisArea.outerHeight() >= settings.limit) return;

					if (!settings.animate.enable || textarea.css('display') != 'block') thisArea.height(scrollW);
					else thisArea.stop().animate({height:scrollW}, settings.animate.duration, settings.animate.callback);

					thisArea.addClass('overflowY');
				}
				else {
					settings.onResize.call(this);

					if (!settings.animate.enable || textarea.css('display') != 'block') thisArea.height(scrollW);
					else thisArea.stop().animate({height:scrollW}, settings.animate.duration, settings.animate.callback);

					thisArea.removeClass('overflowY');
				}
			};


			textarea
				.css({minHeight:$(this).outerHeight(), maxHeight:settings.limit+'px'})
				.off('.ar').on('input.ar propertychange.ar', updateSize);
		});

		return this;
	};
})(jQuery);
